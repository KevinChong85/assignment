/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;

/**
 *
 * @author n4ps03
 */
public class Patient_PersonalInformation {
    public static void main(String[] args) {
    JPanel panel = new JPanel();
    JFrame Patient_PI = new JFrame();
    Patient_PI.setSize(500, 500);
    Patient_PI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Patient_PI.add(panel);
    
    panel.setLayout(null);
    
    JLabel label_CA = new JLabel ("Personal Information");
    label_CA.setBounds(100, 10, 200, 25);
    panel.add(label_CA);
    
    JLabel label_name = new JLabel("Name:");
    label_name.setBounds(10, 50, 80, 25);
    panel.add(label_name);
        
    JTextField text_name = new JTextField();
    text_name.setBounds(100, 50, 165, 25);
    panel.add(text_name);
    
    JLabel label_contact = new JLabel("Contact:");
    label_contact.setBounds(10, 80, 80, 25);
    panel.add(label_contact);
        
    JTextField text_contact = new JTextField();
    text_contact.setBounds(100, 80, 165, 25);
    panel.add(text_contact);
    
    JLabel label_address = new JLabel("Address:");
    label_address.setBounds(10, 110, 80, 25);
    panel.add(label_address);
        
    JTextField text_address = new JTextField();
    text_address.setBounds(100, 110, 165, 25);
    panel.add(text_address);
    
    JLabel label_sex = new JLabel("Sex:");
    label_sex.setBounds(10, 140, 80, 25);
    panel.add(label_sex);
        
    JTextField text_sex = new JTextField();
    text_sex.setBounds(100, 140, 165, 25);
    panel.add(text_sex);
    
    JLabel label_dob = new JLabel("Date of Birth:");
    label_dob.setBounds(10, 170, 80, 25);
    panel.add(label_dob);
        
    JTextField text_dob = new JTextField();
    text_dob.setBounds(100, 170, 165, 25);
    panel.add(text_dob);
    
    JLabel label_ea = new JLabel("Email Address:");
    label_ea.setBounds(10, 200, 100, 25);
    panel.add(label_ea);
        
    JTextField text_ea = new JTextField();
    text_ea.setBounds(100, 200, 165, 25);
    panel.add(text_ea);
    
    JLabel label_PID = new JLabel("Patient ID:");
    label_PID.setBounds(10, 230, 80, 25);
    panel.add(label_PID);
        
    JTextField text_PID = new JTextField();
    text_PID.setBounds(100, 230, 165, 25);
    panel.add(text_PID);
        
    Patient_PI.setVisible(true);     
    
    }
  
}
