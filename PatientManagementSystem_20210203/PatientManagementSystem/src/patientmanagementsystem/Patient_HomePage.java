/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;


/**
 *
 * @author Kevin
 */
public class Patient_HomePage {
    public static void main(String[] args) {
    
    JPanel panel = new JPanel();
    JFrame PatientHomePage = new JFrame();
    PatientHomePage.setSize(500, 350);
    PatientHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    PatientHomePage.add(panel);
    
    panel.setLayout(null);
    
    JLabel Welcome = new JLabel("Welcome! Patient");
    Welcome.setBounds(0, 0, 200, 25);
    panel.add(Welcome);
    
    JButton PI = new JButton("Personal Information");
    PI.setBounds(0, 20, 200, 25);
    panel.add(PI);
          
    JButton MA = new JButton("Make Appointment");
    MA.setBounds(0, 40, 200, 25);
    panel.add(MA);
    
    JButton DF = new JButton("History / Doctor's Feedback");
    DF.setBounds(0, 60, 200, 25);
    panel.add(DF);
    
    PatientHomePage.setVisible(true);

    }
    
}
