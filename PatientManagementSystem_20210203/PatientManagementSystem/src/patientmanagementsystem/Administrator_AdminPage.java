/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;  

/**
 *
 * @author Kevin
 */
public class Administrator_AdminPage {
    public static void main(String[] args) {
    
    JPanel panel = new JPanel();
    JFrame Adminpage = new JFrame();
    Adminpage.setSize(500, 350);
    Adminpage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Adminpage.add(panel);
   
    panel.setLayout(null);
    
    JLabel Welcome = new JLabel("Welcome! Administrator");
    Welcome.setBounds(0, 0, 200, 25);
    panel.add(Welcome);
    
    JButton CreateAC = new JButton("Create Account");
    CreateAC.setBounds(0, 20, 200, 25);
    panel.add(CreateAC);
        
    JButton AddRemoveAC = new JButton("Add / Remove Account");
    AddRemoveAC.setBounds(0, 40, 200, 25);
    panel.add(AddRemoveAC);
    
    JButton Feedback = new JButton("Doctor's Feedback");
    Feedback.setBounds(0, 60, 200, 25);
    panel.add(Feedback);
    
    Adminpage.setVisible(true);
          
        
    }
}