/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;


/**
 *
 * @author Kevin
 */
public class Doctor_HomePage {
    public static void main(String[] args) {
    
    JPanel panel = new JPanel();
    JFrame DoctorHomePage = new JFrame();
    DoctorHomePage.setSize(500, 350);
    DoctorHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    DoctorHomePage.add(panel);
    
    panel.setLayout(null);
    
    JLabel Welcome = new JLabel("Welcome! Doctor");
    Welcome.setBounds(0, 0, 200, 25);
    panel.add(Welcome);
    
    JButton PA = new JButton("Patient's Appointment");
    PA.setBounds(0, 20, 200, 25);
    panel.add(PA);
        
    JButton PH = new JButton("Patient's History");
    PH.setBounds(0, 40, 200, 25);
    panel.add(PH);
    
    JButton CM = new JButton("Create Medicines");
    CM.setBounds(0, 60, 200, 25);
    panel.add(CM);
    
    DoctorHomePage.setVisible(true);

    }
    
}
