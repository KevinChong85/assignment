/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;

/**
 *
 * @author Kevin
 */
public class Secretary_HomePage {
    public static void main(String[] args) {
    
    JPanel panel = new JPanel();
    JFrame SecretaryHomePage = new JFrame();
    SecretaryHomePage.setSize(500, 350);
    SecretaryHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    SecretaryHomePage.add(panel);
    
    panel.setLayout(null);
    
    JLabel Welcome = new JLabel("Welcome! Secretary");
    Welcome.setBounds(0, 0, 250, 25);
    panel.add(Welcome);
    
    JButton APA = new JButton("Approve Patient's Account");
    APA.setBounds(0, 20, 250, 25);
    panel.add(APA);
        
    JButton APSA = new JButton("Approve Patient's Appointment");
    APSA.setBounds(0, 40, 250, 25);
    panel.add(APSA);
    
    JButton OMP = new JButton("Order Medicines Page");
    OMP.setBounds(0, 60, 250, 25);
    panel.add(OMP);
    
    SecretaryHomePage.setVisible(true);

    }
}
