/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;

/**
 *
 * @author Kevin
 */
public class Administrator_CreateAccount {
    public static void main(String[] args) {
    JPanel panel = new JPanel();
    JFrame Admin_CreateAC = new JFrame();
    Admin_CreateAC.setSize(500, 350);
    Admin_CreateAC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Admin_CreateAC.add(panel);
    
    panel.setLayout(null);
    
    JLabel label_CA = new JLabel ("Create Account");
    label_CA.setBounds(100, 10, 200, 25);
    panel.add(label_CA);
      
    JLabel label_name = new JLabel("Name:");
    label_name.setBounds(10, 50, 80, 25);
    panel.add(label_name);
        
    JTextField text_name = new JTextField();
    text_name.setBounds(100, 50, 165, 25);
    panel.add(text_name);
    
    JLabel label_position = new JLabel("Position:");
    label_position.setBounds(10, 80, 80, 25);
    panel.add(label_position);
        
    JTextField text_position = new JTextField();
    text_position.setBounds(100, 80, 165, 25);
    panel.add(text_position);
    
    JLabel label_IDNumber = new JLabel("ID Number:");
    label_IDNumber.setBounds(10, 110, 80, 25);
    panel.add(label_IDNumber);
        
    JTextField text_IDNumber = new JTextField();
    text_IDNumber.setBounds(100, 110, 165, 25);
    panel.add(text_IDNumber);
    
    JLabel label_username = new JLabel("Username:");
    label_username.setBounds(10, 140, 80, 25);
    panel.add(label_username);
        
    JTextField text_username = new JTextField();
    text_username.setBounds(100, 140, 165, 25);
    panel.add(text_username);
    
    JLabel label_password = new JLabel("Password:");
    label_password.setBounds(10, 170, 80, 25);
    panel.add(label_password);
        
    JTextField text_password = new JTextField();
    text_password.setBounds(100, 170, 165, 25);
    panel.add(text_password);
    
    JLabel label_RTpassword = new JLabel("Re-type Password:");
    label_RTpassword.setBounds(10, 200, 150, 25);
    panel.add(label_RTpassword);
        
    JTextField text_RTpassword = new JTextField();
    text_RTpassword.setBounds(125, 200, 165, 25);
    panel.add(text_RTpassword);
    
    JButton btn_submit = new JButton("Submit");
    btn_submit.setBounds(10, 230, 110, 25);
    panel.add(btn_submit);
    
    JButton btn_reset = new JButton("Reset");
    btn_reset.setBounds(125, 230, 110, 25);
    panel.add(btn_reset);
    
    Admin_CreateAC.setVisible(true);   
       
    }
}
