/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package patientmanagementsystem;

import javax.swing.*;


/**
 *
 * @author Kevin Chong
 */
public class PatientManagementSystem {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JPanel panel = new JPanel();
        JFrame main = new JFrame();
        main.setSize(500, 350);
        main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        main.add(panel);
        
        panel.setLayout(null);
        
        JLabel label_title = new JLabel ("Patient Management System");
        label_title.setBounds(100, 10, 200, 25);
        panel.add(label_title);
        
        JLabel label_username = new JLabel("Username:");
        label_username.setBounds(10, 50, 80, 25);
        panel.add(label_username);
        
        JTextField text_username = new JTextField();
        text_username.setBounds(100, 50, 165, 25);
        panel.add(text_username);
        
        JLabel label_password = new JLabel("Password:");
        label_password.setBounds(10, 80, 80, 25);
        panel.add(label_password);
        
        JTextField text_password = new JTextField();
        text_password.setBounds(100, 80, 165, 25);
        panel.add(text_password);
        
        JButton btn_login = new JButton("Login");
        btn_login.setBounds(10, 120, 110, 25);
        panel.add(btn_login);
        
        JButton btn_reset = new JButton("Reset");
        btn_reset.setBounds(150, 120, 110, 25);
        panel.add(btn_reset);
        
        main.setVisible(true);
               
    }
    
}
