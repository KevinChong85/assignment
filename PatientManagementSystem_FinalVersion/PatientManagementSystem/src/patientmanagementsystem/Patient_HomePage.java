/*
 * This page is used for patient to choose which function want to choose.
 * It is included their personal information, make appointment, history and the doctor's feedback.
 * Finally, the patient can logout the system with the logout button.
 */
package patientmanagementsystem;

import javax.swing.*;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Create variable with the different type of frame function.
 */
public class Patient_HomePage extends javax.swing.JFrame{
    
    private static JLabel Welcome;
    private static JButton PI;
    private static JButton MA;
    private static JButton DF;
    private static JButton Logout;   
    
    
    public static void main(String[] args) {
      

    }
    
    // GUI of Patient's homepage
    public Patient_HomePage(){
    JPanel panel = new JPanel();
    JFrame PatientHomePage = new JFrame();
    PatientHomePage.setSize(350, 250);
    PatientHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    PatientHomePage.add(panel);
    
    panel.setLayout(null);
    
    Welcome = new JLabel("Welcome! Patient");
    Welcome.setBounds(100, 0, 200, 25);
    panel.add(Welcome);
    
    // Change page to "Personal Information"
    PI = new JButton("Personal Information");
    PI.setBounds(50, 30, 200, 25);
    panel.add(PI);
    PI.addActionListener((ActionEvent arg0) -> {
        if(PI.isEnabled()) {
            JOptionPane.showMessageDialog(null,"The function of change page is not completed. Please run the file with 'Patient_PersonalInformation.java'.");
            Patient_PersonalInformation Patient_PI= new Patient_PersonalInformation();
        }
    }); 
          
    MA = new JButton("Make Appointment");
    MA.setBounds(50, 60, 200, 25);
    panel.add(MA);
    
    DF = new JButton("History / Doctor's Feedback");
    DF.setBounds(50, 90, 200, 25);
    panel.add(DF);
    
    // Logout the system
    Logout = new JButton("Logout");
    Logout.setBounds(50, 120, 200, 25);
    panel.add(Logout);
    Logout.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            
            if(Logout.isEnabled()) {
                JOptionPane.showMessageDialog(null,"Logout Successful!");
                System.exit(0);
            }
                
        }
    });
    
    // GUI Enabled
    PatientHomePage.setVisible(true);
    
    }
}
