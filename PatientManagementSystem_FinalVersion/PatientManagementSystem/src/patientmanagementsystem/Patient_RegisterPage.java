/*
 * This page is used for patient to register their login account.
 * It is included their personal information such as Name, contact , address & etc...
 * Finally, the patient can login to system after the secretary approved their account.
 */
package patientmanagementsystem;

import javax.swing.*;

/**
 * GUI of Patient's RegisterPage
 */
public class Patient_RegisterPage {
    public static void main(String[] args) {
    JPanel panel = new JPanel();
    JFrame Patient_RegisterPage = new JFrame();
    Patient_RegisterPage.setSize(400, 450);
    Patient_RegisterPage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Patient_RegisterPage.add(panel);
    
    panel.setLayout(null);
    
    JLabel label_CA = new JLabel ("Create Patient's Account");
    label_CA.setBounds(100, 10, 200, 25);
    panel.add(label_CA);
    
    JLabel label_name = new JLabel("Name:");
    label_name.setBounds(10, 50, 80, 25);
    panel.add(label_name);
        
    JTextField text_name = new JTextField();
    text_name.setBounds(100, 50, 165, 25);
    panel.add(text_name);
    
    JLabel label_contact = new JLabel("Contact:");
    label_contact.setBounds(10, 80, 80, 25);
    panel.add(label_contact);
        
    JTextField text_contact = new JTextField();
    text_contact.setBounds(100, 80, 165, 25);
    panel.add(text_contact);
    
    JLabel label_address = new JLabel("Address:");
    label_address.setBounds(10, 110, 80, 25);
    panel.add(label_address);
        
    JTextField text_address = new JTextField();
    text_address.setBounds(100, 110, 165, 25);
    panel.add(text_address);
    
    JLabel label_sex = new JLabel("Sex:");
    label_sex.setBounds(10, 140, 80, 25);
    panel.add(label_sex);
        
    JTextField text_sex = new JTextField();
    text_sex.setBounds(100, 140, 165, 25);
    panel.add(text_sex);
    
    JLabel label_dob = new JLabel("Date of Birth:");
    label_dob.setBounds(10, 170, 80, 25);
    panel.add(label_dob);
        
    JTextField text_dob = new JTextField();
    text_dob.setBounds(100, 170, 165, 25);
    panel.add(text_dob);
    
    JLabel label_ea = new JLabel("Email Address:");
    label_ea.setBounds(10, 200, 100, 25);
    panel.add(label_ea);
        
    JTextField text_ea = new JTextField();
    text_ea.setBounds(100, 200, 165, 25);
    panel.add(text_ea);
    
    JLabel label_username = new JLabel("Username:");
    label_username.setBounds(10, 230, 80, 25);
    panel.add(label_username);
        
    JTextField text_username = new JTextField();
    text_username.setBounds(100, 230, 165, 25);
    panel.add(text_username);
    
    JLabel label_password = new JLabel("Password:");
    label_password.setBounds(10, 260, 80, 25);
    panel.add(label_password);
        
    JTextField text_password = new JTextField();
    text_password.setBounds(100, 260, 165, 25);
    panel.add(text_password);
    
    JLabel label_rtpassword = new JLabel("Retype Password:");
    label_rtpassword.setBounds(10, 290, 150, 25);
    panel.add(label_rtpassword);
        
    JTextField text_rtpassword = new JTextField();
    text_rtpassword.setBounds(125, 290, 165, 25);
    panel.add(text_rtpassword);
    
    JButton btn_submit = new JButton("Submit");
    btn_submit.setBounds(10, 320, 110, 25);
    panel.add(btn_submit);
    
    JButton btn_reset = new JButton("Reset");
    btn_reset.setBounds(125, 320, 110, 25);
    panel.add(btn_reset);
    
    //GUI Enabled
    Patient_RegisterPage.setVisible(true); 
    
    
    }
}
