/* 
 * This page is used for Administrator to choose which function want to choose.
 * It is included Create account, Add & remove account and Doctor Feedback.
 * Finally, the Administrator can logout the system with the logout button.
 */
package patientmanagementsystem;

import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;  

/**
 * Create variable with the different type of frame function.
 */
public class Administrator_AdminPage implements ActionListener{
    private static JLabel Welcome;
    private static JButton CreateAC;
    private static JButton AddRemoveAC;
    private static JButton Feedback;
    private static JButton Logout;
    private static JFrame Adminpage;
    
    public static void main(String[] args) {
      
    }
         
    // Admin Page of GUI       
    public Administrator_AdminPage(){    
    JPanel panel = new JPanel();
    JFrame Adminpage = new JFrame();
    Adminpage.setSize(350, 250);
    Adminpage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Adminpage.add(panel);
   
    panel.setLayout(null);
    
    Welcome = new JLabel("Welcome! Administrator");
    Welcome.setBounds(80, 0, 200, 25);
    panel.add(Welcome);
    
    // This button can change the page to create account.
    CreateAC = new JButton("Create Account");
    CreateAC.setBounds(50, 30, 200, 25);
    panel.add(CreateAC);
    CreateAC.addActionListener((ActionEvent arg0) -> {
        if(CreateAC.isEnabled()) {
            JOptionPane.showMessageDialog(null,"The function of change page is not completed. Please run the file with 'Administrator_CreateAccount.java'.");
            Administrator_CreateAccount AdminCreateAC= new Administrator_CreateAccount();
        }
    }); 
            
    AddRemoveAC = new JButton("Add / Remove Account");
    AddRemoveAC.setBounds(50, 60, 200, 25);
    panel.add(AddRemoveAC);
    
    Feedback = new JButton("Doctor's Feedback");
    Feedback.setBounds(50, 90, 200, 25);
    panel.add(Feedback);
    
    // This button can logout the system.
    Logout = new JButton("Logout");
    Logout.setBounds(50, 120, 200, 25);
    panel.add(Logout);
    Logout.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            
            if(Logout.isEnabled()) {
                JOptionPane.showMessageDialog(null,"Logout Successful!");
                System.exit(0);
            }
                
        }
    });

    // GUI Enabled
    Adminpage.setVisible(true);
    
    }   

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
    



