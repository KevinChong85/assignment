/*
 * This page is used for secretary to choose which function want to choose.
 * It is included the approval of patient account & appointment and order the Medicines Page.
 * Finally, the secretary can logout the system with the logout button.
 */
package patientmanagementsystem;

import javax.swing.*;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *GUI of Secretary_HomePage
 */
public class Secretary_HomePage extends javax.swing.JFrame {
    
    private static JLabel Welcome;
    private static JButton APA;
    private static JButton APSA;
    private static JButton OMP;
    private static JButton Logout;
    
    public static void main(String[] args) {
    
    

    }
    
    public Secretary_HomePage(){
    
    JPanel panel = new JPanel();
    JFrame SecretaryHomePage = new JFrame();
    SecretaryHomePage.setSize(350, 250);
    SecretaryHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    SecretaryHomePage.add(panel);
    
    panel.setLayout(null);
    
    Welcome = new JLabel("Welcome! Secretary");
    Welcome.setBounds(100, 0, 250, 25);
    panel.add(Welcome);
    
    APA = new JButton("Approve Patient's Account");
    APA.setBounds(40, 30, 250, 25);
    panel.add(APA);
        
    APSA = new JButton("Approve Patient's Appointment");
    APSA.setBounds(40, 60, 250, 25);
    panel.add(APSA);
    
    OMP = new JButton("Order Medicines Page");
    OMP.setBounds(40, 90, 250, 25);
    panel.add(OMP);
    
    
    //Logout System
    Logout = new JButton("Logout");
    Logout.setBounds(40, 120, 250, 25);
    panel.add(Logout);
    Logout.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            
            if(Logout.isEnabled()) {
                JOptionPane.showMessageDialog(null,"Logout Successful!");
                System.exit(0);
            }
                
        }
    });
    
    // GUI Enabled
    SecretaryHomePage.setVisible(true);
    
    }
}
