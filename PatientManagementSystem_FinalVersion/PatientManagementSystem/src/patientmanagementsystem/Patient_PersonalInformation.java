/*
 * This page is used for patient to see their personal information.
 * It is included their personal information such as Name, address , contact & etc...
 * Finally, the patient can return to the homepage.
 */
package patientmanagementsystem;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * GUI of Patient's Personal Information 
 */
public class Patient_PersonalInformation implements ActionListener{
    
    private static JButton btn_return;
    
    public static void main(String[] args) {
    JPanel panel = new JPanel();
    JFrame Patient_PI = new JFrame();
    Patient_PI.setSize(400, 425);
    Patient_PI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Patient_PI.add(panel);
    
    panel.setLayout(null);
    
    JLabel label_CA = new JLabel ("Personal Information");
    label_CA.setBounds(100, 10, 200, 25);
    panel.add(label_CA);
    
    JLabel label_name = new JLabel("Name:");
    label_name.setBounds(10, 50, 80, 25);
    panel.add(label_name);
        
    JTextField text_name = new JTextField("Chai Tai Man");
    text_name.setBounds(100, 50, 165, 25);
    text_name.setEnabled(false);
    panel.add(text_name);
    
    JLabel label_contact = new JLabel("Contact:");
    label_contact.setBounds(10, 80, 80, 25);
    panel.add(label_contact);
        
    JTextField text_contact = new JTextField("9876-5432");
    text_contact.setBounds(100, 80, 165, 25);
    text_contact.setEnabled(false);
    panel.add(text_contact);
    
    JLabel label_address = new JLabel("Address:");
    label_address.setBounds(10, 110, 80, 25);
    panel.add(label_address);
        
    JTextField text_address = new JTextField("Flat D, 20/F, Sun Building, 310-315 Lok Hart Road, Wan Chai, Hong Kong");
    text_address.setBounds(100, 110, 165, 25);
    text_address.setEnabled(false);
    panel.add(text_address);
    
    JLabel label_sex = new JLabel("Sex:");
    label_sex.setBounds(10, 140, 80, 25);
    panel.add(label_sex);
        
    JTextField text_sex = new JTextField("M");
    text_sex.setBounds(100, 140, 165, 25);
    text_sex.setEnabled(false);
    panel.add(text_sex);
    
    JLabel label_dob = new JLabel("Date of Birth:");
    label_dob.setBounds(10, 170, 80, 25);
    panel.add(label_dob);
        
    JTextField text_dob = new JTextField("25-11-1994");
    text_dob.setBounds(100, 170, 165, 25);
    text_dob.setEnabled(false);
    panel.add(text_dob);
    
    JLabel label_ea = new JLabel("Email Address:");
    label_ea.setBounds(10, 200, 100, 25);
    panel.add(label_ea);
        
    JTextField text_ea = new JTextField("ctm1994@gmail.com");
    text_ea.setBounds(100, 200, 165, 25);
    text_ea.setEnabled(false);
    panel.add(text_ea);
    
    JLabel label_PID = new JLabel("Patient ID:");
    label_PID.setBounds(10, 230, 80, 25);
    panel.add(label_PID);
        
    JTextField text_PID = new JTextField("P01");
    text_PID.setBounds(100, 230, 165, 25);
    text_PID.setEnabled(false);
    panel.add(text_PID);
    
    // Return to Homepage
    btn_return = new JButton("Return to Homepage");
    btn_return.setBounds(100, 260, 150, 25);
    btn_return.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            
            if(btn_return.isEnabled()) {
                JOptionPane.showMessageDialog(null,"Return Successful!");
                Patient_PI.setVisible(false);
                Patient_HomePage PatientHomePage= new Patient_HomePage();
            }
                
        }
        });
    panel.add(btn_return);
    
    // GUI Enabled
    Patient_PI.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

