/*
 * This page is used for doctor to choose which function want to choose.
 * It is included the patient's appointment, patient's history and create Medicines.
 * Finally, the doctor can logout the system with the logout button.
 */
package patientmanagementsystem;

import javax.swing.*;
import java.awt.EventQueue;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Create variable with the different type of frame function.
 */
public class Doctor_HomePage extends javax.swing.JFrame{
    private static JLabel Welcome;
    private static JButton PA;
    private static JButton PH;
    private static JButton CM;
    private static JButton Logout;
    
    public static void main(String[] args) {
    
    }

    // GUI of Doctor's Homepage
    public Doctor_HomePage(){
        
    JPanel panel = new JPanel();
    JFrame DoctorHomePage = new JFrame();
    DoctorHomePage.setSize(350, 250);
    DoctorHomePage.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    DoctorHomePage.add(panel);
    
    panel.setLayout(null);
    
    Welcome = new JLabel("Welcome! Doctor");
    Welcome.setBounds(100, 0, 200, 25);
    panel.add(Welcome);
    
    PA = new JButton("Patient's Appointment");
    PA.setBounds(50, 30, 200, 25);
    panel.add(PA);
        
    PH = new JButton("Patient's History");
    PH.setBounds(50, 60, 200, 25);
    panel.add(PH);
    
    CM = new JButton("Create Medicines");
    CM.setBounds(50, 90, 200, 25);
    panel.add(CM);
    
    // Logout the system
    Logout = new JButton("Logout");
    Logout.setBounds(50, 120, 200, 25);
    panel.add(Logout);
    Logout.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent arg0) {
            
            if(Logout.isEnabled()) {
                JOptionPane.showMessageDialog(null,"Logout Successful!");
                System.exit(0);
            }
                
        }
    });
    
    // GUI Enabled
    DoctorHomePage.setVisible(true);
    
    }
}
    

