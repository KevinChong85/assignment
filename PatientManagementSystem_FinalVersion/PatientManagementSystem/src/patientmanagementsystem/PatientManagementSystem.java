/* Hello! Welcome to Patient Management System.
 * This page is used for the user to login the system.
 * It is included Administrator, Doctor, Patient and Secretary.
 */
package patientmanagementsystem;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class PatientManagementSystem implements ActionListener {
    
    private static JLabel label_title;
    private static JLabel label_username;
    private static JTextField text_username;
    private static JLabel label_password;
    private static JPasswordField text_password;
    private static JButton btn_login;
    private static JFrame main;
        
          
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    
       
    //GUI of Login page 
        JPanel panel = new JPanel();
        JFrame main = new JFrame();
        main.setSize(350, 250);
        main.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        main.add(panel);
        
        panel.setLayout(null);
        
        label_title = new JLabel ("Patient Management System");
        label_title.setBounds(100, 10, 200, 25);
        panel.add(label_title);
        
        label_username = new JLabel("Username:");
        label_username.setBounds(10, 50, 80, 25);
        panel.add(label_username);
        
        text_username = new JTextField();
        text_username.setBounds(100, 50, 165, 25);
        panel.add(text_username);
        
        label_password = new JLabel("Password:");
        label_password.setBounds(10, 80, 80, 25);
        panel.add(label_password);
        
        text_password = new JPasswordField();
        text_password.setBounds(100, 80, 165, 25);
        text_password.setEchoChar('*');
        panel.add(text_password);
        
        btn_login = new JButton("Login");
        btn_login.setBounds(100, 120, 110, 25);
        btn_login.addActionListener(new PatientManagementSystem()); 
        panel.add(btn_login);
        String login = btn_login.getText();
        
        // GUI Enabled
        main.setVisible(true);    
        
    }   
    
    //Account Information & Authentication
    @Override
    public void actionPerformed(ActionEvent Login) {
    String admin_user = "Admin01";
    String admin_pw = "A01Psw0rd";
    String patient_user = "Patient01";
    String patient_pw = "P01Psw0rd";
    String doctor_user = "Doctor01";
    String doctor_pw = "D01Psw0rd";
    String secretary_user = "Secretary01";
    String secretary_pw = "S01Psw0rd";
            
    String username = text_username.getText();
    String password = text_password.getText();
        
    if (admin_user.equals(username) && admin_pw.equals(password)) {
        JOptionPane.showMessageDialog(null,"Login Successful!");            
        Administrator_AdminPage adminpage= new Administrator_AdminPage();
    }
    else if (patient_user.equals(username) && patient_pw.equals(password)) {
        JOptionPane.showMessageDialog(null,"Login Successful!");
        Patient_HomePage PatientHomePage= new Patient_HomePage();
    }
    else if(doctor_user.equals(username) && doctor_pw.equals(password)) {
        JOptionPane.showMessageDialog(null,"Login Successful!");
        Doctor_HomePage DoctorHomePage= new Doctor_HomePage();
    }
    else if(secretary_user.equals(username) && secretary_pw.equals(password)) {
        JOptionPane.showMessageDialog(null,"Login Successful!");
        Secretary_HomePage SecretaryHomePage= new Secretary_HomePage();
    }
    else {
        JOptionPane.showMessageDialog(null,"Invalid Username & Password! Please try again."); 
        text_username.setText("");
        text_password.setText("");
    }
         
   }
}
    

        
        
        
        
        
        
    
    
    
    
    

    
    
   





