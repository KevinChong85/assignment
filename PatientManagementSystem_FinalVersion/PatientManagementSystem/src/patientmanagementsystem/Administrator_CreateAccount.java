

/*
 * This page is used for create the business account.
 * The Administrator can input the doctor or Secretary's personal information to create account.
 */
package patientmanagementsystem;

import java.awt.event.ActionEvent;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;

/**
 * Create variable with the different type of frame function.
 */
public class Administrator_CreateAccount extends javax.swing.JFrame {
        
    private static JLabel label_CA;
    private static JLabel label_name;
    private static JLabel label_position;
    private static JLabel label_IDNumber;
    private static JLabel label_username;
    private static JLabel label_password;
    private static JLabel label_RTpassword;
    private static JButton btn_submit;
    private static JButton btn_reset;
    private static JTextField text_name;
    private static JTextField text_position;
    private static JTextField text_IDNumber;
    private static JTextField text_username;
    private static JTextField text_password;
    private static JTextField text_RTpassword;
    
        
    
    
    public static void main(String[] args) throws IOException {
        
    // GUI of Create Account
    JPanel panel = new JPanel();
    JFrame Admin_CreateAC = new JFrame();
    Admin_CreateAC.setSize(500, 350);
    Admin_CreateAC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    Admin_CreateAC.add(panel);
    
    panel.setLayout(null);
    
    label_CA = new JLabel ("Create Account");
    label_CA.setBounds(100, 10, 200, 25);
    panel.add(label_CA);
      
    label_name = new JLabel("Name:");
    label_name.setBounds(10, 50, 80, 25);
    panel.add(label_name);
        
    text_name = new JTextField();
    text_name.setBounds(100, 50, 165, 25);
    panel.add(text_name);
    
    label_position = new JLabel("Position:");
    label_position.setBounds(10, 80, 80, 25);
    panel.add(label_position);
        
    text_position = new JTextField();
    text_position.setBounds(100, 80, 165, 25);
    panel.add(text_position);
    
    label_IDNumber = new JLabel("ID Number:");
    label_IDNumber.setBounds(10, 110, 80, 25);
    panel.add(label_IDNumber);
        
    text_IDNumber = new JTextField();
    text_IDNumber.setBounds(100, 110, 165, 25);
    panel.add(text_IDNumber);
    
    label_username = new JLabel("Username:");
    label_username.setBounds(10, 140, 80, 25);
    panel.add(label_username);
        
    text_username = new JTextField();
    text_username.setBounds(100, 140, 165, 25);
    panel.add(text_username);
    
    label_password = new JLabel("Password:");
    label_password.setBounds(10, 170, 80, 25);
    panel.add(label_password);
        
    text_password = new JTextField();
    text_password.setBounds(100, 170, 165, 25);
    panel.add(text_password);
    
    label_RTpassword = new JLabel("Re-type Password:");
    label_RTpassword.setBounds(10, 200, 150, 25);
    panel.add(label_RTpassword);
        
    text_RTpassword = new JTextField();
    text_RTpassword.setBounds(125, 200, 165, 25);
    panel.add(text_RTpassword);
    
    btn_submit = new JButton("Submit");
    btn_submit.setBounds(10, 230, 110, 25);
    btn_submit.addActionListener(new PatientManagementSystem());
    panel.add(btn_submit);
    
    btn_reset = new JButton("Reset");
    btn_reset.setBounds(125, 230, 110, 25);
    panel.add(btn_reset);
    
    // GUI Enabled
    Admin_CreateAC.setVisible(true);    
    
    }
    
    /**
     *
     * This part is used for export the information to .txt file and save the personal information.
     *
     */
        
    public void actionPerformed(ActionEvent Submit) throws IOException{
        String name = text_name.getText();
        String position = text_position.getText();
        String idnumber = text_IDNumber.getText();
        String username = text_username.getText();
        String password = text_password.getText();
        String rtpassword = text_RTpassword.getText(); 
        
        File personalfile = new File("PersonInf.txt");
        FileWriter txt = new FileWriter(personalfile, true);
        PrintWriter pw = new PrintWriter(txt);
        JOptionPane.showMessageDialog(null,"Success");
        
        pw.println(name);
        pw.println(position);
        pw.println(idnumber);
        pw.println(username);
        pw.println(password);
        pw.println(rtpassword);
        
        pw.close();  
     
    
    
    }
}
